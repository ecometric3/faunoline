from django.contrib import admin

from animalkingdom.models import (
    Animal,
    AnimalClass, 
    AnimalOrder,
    AnimalFamily,
    AnimalGenus, 
    AnimalSpecies,
    AnimalOrigin,
    AnimalName
)


@admin.register(Animal)
class AnimalAdmin(admin.ModelAdmin):
    pass


@admin.register(AnimalClass)
class AnimalClassAdmin(admin.ModelAdmin):
    pass


@admin.register(AnimalOrder)
class AnimalOrderAdmin(admin.ModelAdmin):
    pass

@admin.register(AnimalFamily)
class AnimalFamilyAdmin(admin.ModelAdmin):
    pass

@admin.register(AnimalGenus)
class AnimalGenusAdmin(admin.ModelAdmin):
    pass


@admin.register(AnimalSpecies)
class AnimalSpeciesAdmin(admin.ModelAdmin):
    pass

@admin.register(AnimalOrigin)
class AnimalOriginAdmin(admin.ModelAdmin):
    pass

@admin.register(AnimalName)
class AnimalNameAdmin(admin.ModelAdmin):
    pass

