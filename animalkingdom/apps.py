from django.apps import AppConfig


class AnimalkingdomConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'animalkingdom'
