from django.db import models

from django.db import models
from animalkingdom.utils import definitions
#from smart_selects.db_fields import ChainedForeignKey

"""
Animal Kingdom Models

version 2.0: models related to Taxonomy. 
"""

class Animal(models.Model):
    """
    Model: Animal
    + Description: root model for associated taxonomies
        - Atributes:
        - id: auto
        - scientific_name: name of the Phylum. 
    """

    # Pk auto id
    kingdom = models.CharField(max_length=10, default="CHORDATA")
    phylum = models.CharField(max_length=10, default="ANIMALIA")

    def __str__(self):
        # Model id k: kingdom id p: phylum
        return '(id:%s)(K:%s P:%s) ' % (
                self.pk,
                self.kingdom,
                self.phylum
                )
        
    class Meta:
        verbose_name_plural = "1. Animals"
    

class AnimalName(models.Model):
    # pk auto id
    # Fk
    animal  = models.ForeignKey(
            Animal,
            on_delete = models.CASCADE)
    # Atributes
    name = models.CharField(
        max_length=25,
        default="unidentified")

    def __str__(self):
        return '(id:%s)(%s) ' % (
                self.pk,
                self.name
                )
        
    class Meta:
        verbose_name_plural = "2. Animal Names"

        
class AnimalClass(models.Model):
    """
    Model: AnimalClass
    + Description: Animal Classes
        - Atributes:
        - id: auto
        - scientific_name: scientific name of the animal class  
        - common_name: common name of the animal class
    """

    # Pk auto id
    # Fk
    animal_name  = models.ForeignKey(
            AnimalName,
            on_delete = models.CASCADE)
    # Atributes
    scientific_name = models.CharField(
        max_length=16,
        choices = definitions.sci_animal_classes,
        default="unidentified")
    common_name = models.CharField(
        max_length=16,
        choices = definitions.comm_animal_classes,
        default="unidentified")

    def __str__(self):
        #(id) (animal name - class_scientific_name)
        return '(id:%s)(%s - %s)' % (
                self.pk,
                self.animal_name.name,
                self.scientific_name
                )
        
    class Meta:
        verbose_name_plural = "3. Animal Classes"


class AnimalOrder(models.Model):
    """
    Model: AnimalOrder
    + Description: Animal Orders
        - Atributes:
        - id: auto
        - scientific_name: scientific name of the animal class  
        - common_name: common name of the animal class
    """

    # Pk auto id
    # Fk
    animal_class  = models.ForeignKey(
            AnimalClass,
            on_delete = models.CASCADE)
    scientific_name = models.CharField(
        max_length=24,
        choices = definitions.sci_animal_orders,
        default="unidentified")
    common_name = models.CharField(
        max_length=24,
        choices = definitions.comm_animal_orders,
        default="unidentified")

    def __str__(self):
        #(id) (animal name - order_scientific_name)
        return '(id:%s)(%s - %s)' % (
                self.pk,
                self.animal_class.animal_name.name,
                self.scientific_name
                )
        
    class Meta:
        verbose_name_plural = "4. Animal Orders"


class AnimalFamily(models.Model):
    """
    Model: AnimalFamily
    + Description: Animal Families
    + Atributes:
        - id: auto
        - scientific_name: scientific name of the animal class  
    """
    
    # Pk auto id
    # Fk
    animal_order  = models.ForeignKey(
            AnimalOrder,
            on_delete = models.CASCADE)
    scientific_name = models.CharField(
        max_length=18,
        choices = definitions.sci_animal_families,
        default="unidentified")

    def __str__(self):
        #(id) (animal name - Family_scientific_name)
        return '(id:%s)(%s - %s)' % (
                self.pk,
                self.animal_order.animal_class.animal_name.name,
                self.scientific_name
                )
        
    class Meta:
        verbose_name_plural = "5. Animal Families"


class AnimalGenus(models.Model):
    """
    Model: AnimalGenus
    + Description: Animal Genuses
    + Atributes:
        - id: auto
        - scientific_name: scientific name of the animal class  
    """

    # Pk auto id
    # Fk
    animal_family  = models.ForeignKey(
            AnimalFamily,
            on_delete = models.CASCADE)
    scientific_name = models.CharField(
        max_length=19,
        choices = definitions.sci_animal_genuses,
        default="unidentified")

    def __str__(self):
        #(id) (animal name - Genus_scientific_name)
        return '(id:%s)(%s - %s)' % (
                self.pk,
                self.animal_family.animal_order.animal_class.animal_name.name,
                self.scientific_name
                )
        
    class Meta:
        verbose_name_plural = "6. Animal Genuses"


class AnimalSpecies(models.Model):
    """
    Model: AnimalSpecies
    + Description: Animal Species
    + Atributes:
        - id: auto
        - scientific_name: scientific name of the animal class  
    """

    # Pk auto id
    # Fk
    animal_genus  = models.ForeignKey(
            AnimalGenus,
            on_delete = models.CASCADE)
    scientific_name = models.CharField(
        max_length=40,
        choices = definitions.sci_animal_species,
        default="unidentified")
    common_name = models.CharField(
        max_length=40,
        choices = definitions.comm_animal_species,
        default="unidentified")

    def __str__(self):
        #(id) (animal name - species_scientific_name)
        return '(id:%s)(%s - %s)' % (
            self.pk,
            self.animal_genus.animal_family.animal_order.animal_class.animal_name.name,
            self.scientific_name
            )
        
    class Meta:
        verbose_name_plural = "6. Animal Species"


class AnimalOrigin(models.Model):
    """
    Model: AnimalOrigin
    + Description: Animal Origins
    + Atributes:
        - id: auto
        - common_name: common nameof the animal Origin  
    """

    # Pk auto id
    # Fk
    animal_species  = models.ForeignKey(
            AnimalSpecies,
            on_delete = models.CASCADE)
    common_name = models.CharField(
        max_length=16,
        choices = definitions.comm_animal_origins,
        default="unidentified")

    def __str__(self):
        #(id) (animal name - origin_scientific_name)
        return '(id:%s)(%s - %s)' % (
            self.pk,
            self.animal_species.animal_genus.animal_family.animal_order.animal_class.animal_name.name,
            self.common_name
            )
        
    class Meta:
        verbose_name_plural = "7. Animal Origins"

