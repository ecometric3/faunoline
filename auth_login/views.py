from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from django.shortcuts import render, redirect
# authenticate, login and logout
from django.contrib import auth
from django.contrib import messages
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.urls import reverse
import logging

# imports from the auth_login aplication
#from auth_login.forms import UserClientForm
#from auth_login.models import Profile
from client.models import (
    UserXStation,
    Station
)


def login(request):
    """
    + Función login
        - Descripción: recibe los datos de ingreso del usuario y los valida.
                    Si los datos son válidos lo redirige, sino, error.
        + Atributos:
            - template_name: template asociada a la función
            - data: datos a entregar al template
            - username: usuario ingresado
            - password: contraseña ingresada
    """
    template_name = 'login.html'
    data = {}
  
    auth.logout(request)

    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        
        user = auth.authenticate(
            username=username,
            password=password
        )
        if user is not None:
            # Flujo si el usuario existe. Entrega el primer id estación de la lista
            if user.is_active:
                # uUsuario validado, redirigir.
                #print(f"DATOS USUARIO QUE INTENTA HACER LOGIN: {user.id}")
                user_x_station = UserXStation.objects.filter(user_id=user.id)
                #print(f"DATOS USUARIO x STATION QUE INTENTA HACER LOGIN: {user_x_station}")
                loggin_station = user_x_station[0].station.id
                #print(f"DATOS ESTACION QUE INTENTA HACER LOGIN: {loggin_station}")
                auth.login(request, user,)

                return redirect('client:notifications', station_id=loggin_station)

            else:
                # Datos erróneos
                messages.add_message(
                    request,
                    messages.ERROR,
                    'Usuario o Contraseña incorrectos.'
                )
        else:
            # Usuario no existe
            messages.add_message(
                request,
                messages.ERROR,
                'Usuario o Contraseña incorrectos.'
            )

    return render(request, template_name, data)


#dmin/client/monitoring/