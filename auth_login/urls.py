from django.urls import path
from auth_login import views


app_name = 'auth_login'

urlpatterns = [
    path('',views.login, name='login'),
    #path('logout', views.logout, name='logout'),
]