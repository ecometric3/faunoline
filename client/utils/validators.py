from django.core import validators

"""
+ validators.py
    - Objetivo: validadores de objetos usados por los modelos
    + Validadores:
        - target: especie objetivo que se espera encontrar
        - model: modelo del hardware asociado
        - log detail: detalle del estado de el hardware
        - notes: apuntes referentes a lo observado en img/video capturado
"""

# [Station Model] - target validator
target_minmsg = "La lista de especies objetivo debe tener mínimo 10 caracteres"
target_maxmsg = "La lista de especies objetivo debe tener máximo 100 caracteres"

validator_target_length = [
    validators.MinLengthValidator(10, target_minmsg),
    validators.MaxLengthValidator(100, target_maxmsg)]

# [Hardware Model] - model validator
hardware_minmsg = "El modelo del equipo debe tener mínimo 2 caracteres"
hardware_maxmsg = "El modelo del equipo  debe tener máximo 45 caracteres"

validator_model_length = [
    validators.MinLengthValidator(2, hardware_minmsg),
    validators.MaxLengthValidator(45, hardware_maxmsg)]

# [HardwareLog Model] - log detail
log_minmsg = "El detalle debe tener mínimo 10 caracteres"
log_maxmsg = "El detalle debe tener máximo 100 caracteres"

validator_log_length = [
    validators.MinLengthValidator(10, log_minmsg),
    validators.MaxLengthValidator(100, log_maxmsg)]

# [Monitoring Model] - notes
notes_minmsg = "El detalle debe tener mínimo 10 caracteres"
notes_maxmsg = "El detalle debe tener máximo 100 caracteres"

validator_notes_length = [
    validators.MinLengthValidator(10, notes_minmsg),
    validators.MaxLengthValidator(100, notes_maxmsg)]