from rest_framework import serializers
from client.models import Monitoring

class HomeClientAPIViewSerializer(serializers.Serializer):

    """
    + Clase HomeClientAPIViewSerializer
        - Descripción: Recibe los datos desde Raspberry, verificando que lo 
                    recibido tiene el formato correcto.
        + Atributos:
            - station_id: estación asociada a la alerta. Es un valor constante 
                        que entrega la Raspberry que se conecta.
            - animal_alert: boolean. 
                - True: es un animal
                - False: es monitoreo de validación de funcionamiento
            - monitoring_datetime: fecha hr de monitoreo
            - image_url: link a s3 de la imagen almacenada
            - video_url: link del video almacenado en s3
            - (*) url_audio: aun en desarrollo
        + Ejemplo de .json a enviar desde Raspberry:
            {
                "station_id": 3,
                "animal_alert": "True/False",
                "image_url": "https://testimgrepository.s3.amazonaws.com/...",
                "video_url": "https://testimgrepository.s3.amazonaws.com/...",
                "monitoring_time": "YYYY-mm-dd hh:mm"
            }
    """
    station_id = serializers.IntegerField()
    record = serializers.BooleanField()
    image_url = serializers.URLField(required=False, default="")
    video_url = serializers.URLField(required=False, default="")
    monitoring_datetime = serializers.DateTimeField()

    def create(self, validated_data):
        """
        + Función create:
            - Descripción: crea nuevo registro en el modelo Monitoring
                            con los datos validados por el serializer.
        """
        return Monitoring.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        + Función update:
            - Descripción: actualiza registro en el modelo Monitoring
                            con los datos validados por el serializer.
        """
        instance.station_id = validated_data.get(
                            'station_id', instance.station_id)
        instance.record = validated_data.get(
                            'record', instance.record)
        instance.image_url = validated_data.get(
                            'image_url', instance.image_url)
        instance.video_url = validated_data.get(
                            'video_url', instance.video_url)
        instance.monitoring_datetime = validated_data.get(
                            'monitoring_datetime', instance.monitoring_datetime)        
        instance.save()
        return instance