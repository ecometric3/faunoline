# Login
from django.contrib.auth.decorators import login_required, permission_required
# TODO: Delete? from django.contrib import messages

# Template communication
from django.http import JsonResponse
from django.http import HttpResponse
from django.shortcuts import (
    render, 
    reverse,
    redirect,
    get_object_or_404, 
    HttpResponseRedirect)

# Models import: Manager, Client, AnimalKingdom
from manager.models import Organization
from client.models import (
    DetectedElement, Monitoring,
    UserXStation, Station,
    DetectedAnimal
)
from animalkingdom.models import (
    Animal, AnimalName,
    AnimalClass, AnimalOrder,
    AnimalFamily, AnimalGenus,
    AnimalSpecies, AnimalOrigin
)

# Forms import: Monitoring and Detected element.
from client.forms import (
    MonitoringForm,
    DetectedElementForm
)

# Miscellaneous
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from datetime import datetime
import csv


# Base 
@login_required
def home_client_base(request):
    """
    + Funtion: home_client_base
        - Description: returns the render of the base template, used by the 
               core templates (notifications, gallery, station, records)     
    """

    data = {}
    template_name = '../templates/client_base.html'
    return render(request, template_name, data)


# [LOGED USER DATA]
def session_data(login_user, station_id):
    """
    + Function session_data
        - Description: receives the connected user, filter the user / station / 
                    organization data, and returns it.
        + Parameters:
            - login_user: connected user through auth app
            - station_id: id of the station selected by the user and, from 
                        which the data is required
            - user_stations: [objects array] list of stations objects 
                            related to the logged user
            - selected_station: [object] selected station
        + Variables:
            - user_x_station: [object] user and stations data related to the 
                            logged user. 
            + data: [dictionary] data to be returned
                - logged_user: [object] user data of the logged user
                - logged_station: [object] data from the oldest (or unique) 
                            station related to the logged user
                - organization_name: [string] organization name related 
                            to the logged user
                - user_stations [array]
                - selected_station: [int] id of the selected station
                - station_name: [string] name of the selected station
        + Return:
            - data
    """

    data = {}
    # Filter the Station objects related to the logged user
    user_x_station = UserXStation.objects.filter(user=login_user)
    # Get login related data
    data['logged_user'] = user_x_station[0].user
    data['logged_station'] = user_x_station[0].station
    data['organization_name'] = data['logged_station'].fauno_project.organization.name
    # Get array of stations
    user_stations = []
    for ustation in user_x_station:
        user_stations.append(ustation.station)
    data['user_stations'] = user_stations   
    # Get the name of the project related to the selected station
    selected_station = UserXStation.objects.filter(
                                            user = login_user).filter(
                                            station_id = station_id)
    data['selected_station'] = station_id
    data['station_name'] = (selected_station[0].station.fauno_project.name[9:]).upper()
    data['station_target'] = (selected_station[0].station.target).upper()
    print(f"[SESSION DATA]: received data\nuser:{login_user}\nstation_id:{station_id}\nReturned data: {data}\n")

    return data
    

# [NOTIFICATIONS DATA]
@login_required
def notifications(request, station_id):
    """
    + Function notifications
        - Description: receives the logged user data and a station id.
                    If this function was called by auth app, the station id 
                    will be the oldest (or unique) station related to the user.
                    Otherwise, the station id will be the one that the user
                    select through a combobox in front-end.
        + Sub-functions:
            - session_data: in order to get all the data related, the function
                    will call to other function named 'session_data'
            - render: returns an httpresponse in which the request data, 
                    template and data dictionary are found
        + Parameters:
            - request: requested data such as user data, session etc.
            - station_id: station id that provides access to the data that the
                    user needs to load
        + Variables:
            - data: [dictionary] data delivered to front-end
                - logged_user
                - logged_station
                - organization_name
                - user_stations
                - selected_station
                - station_name
                - monitoring: [object] monitoring data from the selected 
                            station 

            - template_name: path to the template that will use the data inser-
                    ted in the dictionary named 'data'
    """
    
    data = {}
    # Get the data related to the logged user
    data = session_data(request.user, station_id)

    # Get the last 10 monitoring data where record = True 
    data['monitoring'] = Monitoring.objects.filter(
                                            station_id = station_id).filter(
                                            record = True).order_by('-id')[:10]

    #Send data to front-end
    template_name = '../templates/client_notifications.html'
    return render(request, template_name, data)


# [STATION DATA]
@login_required
def station_data(request, station_id):
    """
    + Function station_data
        - Description: receives request data and a station id. The station id 
                    will be the one that the user select through a combobox in 
                    front-end. This id will be given to this view by the 
                    template through url.
        + Sub-functions:
            - session_data: in order to get all the data related, the function
                    will call to other function named 'session_data'
            - render: returns an httpresponse in which the request data, 
                    template and data dictionary are found
        + Parameters:
            - request: requested data such as user data, session etc.
            - station_id: station id that provides access to the data that the
                    user needs to load
        + Variables:
            - data: [dictionary] data delivered to front-end
                - logged_user
                - logged_station
                - organization_name
                - user_stations
                - selected_station
                - station_name
                - last_alert: [object] the last monitoring data from the 
                            selected station 

            - template_name: path to the template that will use the data inser-
                    ted in the dictionary named 'data'
    """

    data = {}
    # Get the data related to the logged user
    data = session_data(request.user, station_id)

    # Get the last monitoring data from record = True 
    data['last_alert'] = Monitoring.objects.filter(
                            station_id = station_id).filter(
                                record = True).order_by('-id')[:1][::-1]
    
    #Send data to front-end
    template_name = '../templates/client_station.html'
    return render(request, template_name, data)


# [GALLERY DATA] - all records  from the station
@login_required
def gallery(request, station_id):
    """
    + Function Gallery
        - Description: shows a gallery of selected photos from the choses
                    station. These selections must be made by the user through 
                    the monitoring record update.
                    The station id will be given to this view by the 
                    template through url.
        + Sub-functions:
            - session_data: in order to get all the data related, the function
                    will call to other function named 'session_data'
            - Paginator: allows to manage paginated data, in this case, the
                    monitoring pages to show selected photos
            - render: returns an httpresponse in which the request data, 
                    template and data dictionary are found
        + Parameters:
            - request: requested data such as user data, session etc.
            - station_id: station id that provides access to the data that the
                    user needs to load
        + Variables:
            - data: [dictionary] data delivered to front-end
                - logged_user
                - logged_station
                - organization_name
                - user_stations
                - selected_station
                - station_name
                - selected_monitoring: [object] the monitoring objects where 
                                    the atribute "in_gallery" is true.
                - page_monitoring: output of the Paginator class.

            - template_name: path to the template that will use the data inser-
                    ted in the dictionary named 'data'
        
    """
    data = {}
    # Get the data related to the logged user
    data = session_data(request.user, station_id)

    # Get the monitoring data  where in_gallery = True
    data['selected_monitoring'] = Monitoring.objects.filter(
                                            station_id = station_id).filter(
                                            in_gallery=True)

    # Photo gallery pagination:
    page = request.GET.get('page', 1)
    # Paginator(selected monitoring object list, items per page)
    paginator = Paginator(data['selected_monitoring'][::-1],12)
    # Set the pagination
    try:
        # Returns a page object of the given index (page)
        data['page_monitoring'] = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page 
        data['page_monitoring'] = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        data['page_monitoring'] = paginator.page(paginator.num_pages)

    #Send data to front-end
    template_name = '../templates/client_gallery.html'
    return render(request, template_name, data)


# [GALLERY DATA] - selected record
@login_required
def gallery_detail(request, station_id, gallery_id):
    """
    + Function gallery_detail
        - Description: shows all the information related to the photo selected
                    at the gallery. 
        + Sub-functions:
            - session_data: in order to get all the data related, the function
                    will call to other function named 'session_data'
            - render: returns an httpresponse in which the request data, 
                    template and data dictionary are found
        + Parameters:
            - request: requested data such as user data, session etc.
            - station_id: station id that provides access to the data that the
                    user needs to load
        + Variables:
            - data: [dictionary] data delivered to front-end
                - logged_user
                - logged_station
                - organization_name
                - user_stations
                - selected_station
                - station_name
                - monitoring:[object] monitoring record selected to show
                - detection: [object] monitoring detections
            - template_name: path to the template that will use the data inser-
                    ted in the dictionary named 'data'
    """
    data = {}
    # Get the data related to the logged user
    data = session_data(request.user, station_id)

    # Get the monitoring data from the selected record
    data['monitoring'] = Monitoring.objects.filter(
                                        id = gallery_id)
    # Get the related detections from the selected record
    data['detection'] =  DetectedAnimal.objects.filter(
                                        monitoring=data['monitoring'][0].id)

    #Send data to front-end
    template_name = '../templates/client_gallery_detail.html'   
    return render(request, template_name, data)


# [MONITORING DATA] - all records from the station
@login_required
def monitoring_processing(request, station_id):
    """
    + Function monitoring_processing
        - Description: shows a list of videos related to the station selected.
            Each video shows its id, the record time and 4 buttons that allows 
            to the user:
            - Update monitoring data sended by RPi and recieved through API
            - Create taxonomy data for each animal detected, if there was any
            - Download image
            - Download video
        + Sub-functions:
            - session_data: in order to get all the data related, the function
                    will call to other function named 'session_data'
            - render: returns an httpresponse in which the request data, 
                    template and data dictionary are found
        + Parameters:
            - request: requested data such as user data, session etc.
            - station_id: station id that provides access to the data that the
                    user needs to load
        + Variables:
            - data: [dictionary] data delivered to front-end
                - logged_user
                - logged_station
                - organization_name
                - user_stations
                - selected_station
                - station_name
                - monitoring:[object] pending monitoring records of the station
            - template_name: path to the template that will use the data inser-
                    ted in the dictionary named 'data'
    """

    data = {}
    # Get the data related to the logged user
    data = session_data(request.user, station_id)

    # Get the monitoring data  where record = True and the status != reviewed
    data['monitoring'] = Monitoring.objects.filter(
                        station_id = station_id).filter(
                        record = True).filter(
                        status__in = ('viewed',
                                    'pending_monitoring',
                                    'pending_taxonomy')).order_by('-id')[::-1]
                


    # Monitoring records pagination:
    page = request.GET.get('page', 1)
    # Paginator(selected monitoring object list, items per page)
    paginator = Paginator(data['monitoring'][::-1],20)
    # Set the pagination
    try:
        # Returns a page object of the given index (page)
        data['monitoring'] = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page 
        data['monitoring'] = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        data['monitoring'] = paginator.page(paginator.num_pages)

    #Send data to front-end
    template_name = '../templates/client_monitoring_processing.html'
    return render(request, template_name, data)


@login_required
def monitoring_update(request, monitoring_id):
    """
    + Funtion: monitoring_update
        - Description: allows the user to update the monitoring data through
                    a form, accessed on the previews page. The form is
                    embedded in a modal and has more options to complement
                    the data sended by RPi, which are: false positive, number
                    of elements detected, human presence, if the user
                    wants to show this in the gallery, notas and review status. 
                    The option "Revisar estado" (review status) has the 
                    following alternatives:
                    - Visto/Viewed: (default option) the record has no changes
                    - Monitoreo pendiete/pending monitoring: the monitoring
                            data is outdated, but the taxonomy is ready
                    - Taxonomía pendiente/pending taxonomy: the taxonomy data
                            is outdated, but the monitoring is ready
                    - Revisado/reviewed: if the monitoring status is reviewed, 
                            when the user refreshes the page, the record video
                            will disappear, and the related animal detections
                            can be seen in the record detail page, as well as 
                            the detection list module (Taxonomía->detecciones)
        + Sub-functions:
            - get_object_or_404: Calls get() on a given model manager, and
                    returns the object or raises Http404 if the is none.
            - is_valid(): model form validation
            - save(): creates/saves an object from the data bound to the form
            - render: returns an httpresponse in which the request data, 
                    template and data dictionary are found
        + Parameters:
            - request: requested data such as user data, session etc.
            - monitoring_id: monitoring id provides access to the existing
                    monitoring data that the form will use to update
        + Variables:
            - obj: get a monitoring object related to monitoring_id parameter,
                if thre aren't existing objects, delivers a 404 error
            - monitoring_form: gets a MonitoringForm instance
            - data: [dictionary] data delivered to front-end
                - response: response from the attempt to update monitoring data
                - form: MonitoringForm Object 
                - monitoring_id: Monitoring id of the object to update
            - template_name: path to the template that will use the data inser-
                    ted in the dictionary named 'data'
    """

    data ={}
      # Fetch the object related to monitoring_id parameter
    obj = get_object_or_404(Monitoring, id = monitoring_id)
  
    # Pass the object as instance in form MonitoringForm
    monitoring_form = MonitoringForm(request.POST or None, instance = obj)
  
    # Try to save the data from the form and return Response
    if monitoring_form.is_valid():
        monitoring_form.save()
        data['response']="Actualizado"
    elif monitoring_form.is_valid == False:
        data['response']="Error, no se ha realizado actualización"
        # return HttpResponseRedirect("/"+str(monitoring_id))
  
    # Add form instance to dictionary
    data['form'] = monitoring_form
  
    # Add monitoring id to dictionary
    data['monitoring_id'] = monitoring_id

    #Send data to front-end (modal)
    template_name = '../templates/client_monitoring_update.html'
    return render(request, template_name, data)


# [DETECTION DATA] - List
@login_required
def detection_list(request, station_id):
    """
    + Function detection_list
        - Description: shows the list of detections filled in the taxomy form,
                    which monitoring status = "reviewed"
        + Sub-functions:
            - session_data: in order to get all the data related, the function
                    will call to other function named 'session_data'
            - render: returns an httpresponse in which the request data, 
                    template and data dictionary are found
        + Parameters:
            - request: requested data such as user data, session etc.
            - station_id: station id that provides access to the data that the
                    user needs to load
        + Variables:
            - data: [dictionary] data delivered to front-end
                - logged_user
                - logged_station
                - organization_name
                - user_stations
                - selected_station
                - station_name
                - detections:[object] detection list
            - template_name: path to the template that will use the data inser-
                    ted in the dictionary named 'data'
    """
    
    data = {}
    # Get the data related to the logged user
    data = session_data(request.user, station_id)
    
    # Detection objects filter
    data['detections']= DetectedAnimal.objects.filter(
                                        monitoring__station__id=station_id)

    #Send data to front-end
    template_name = '../templates/client_detection_list.html'
    return render(request, template_name, data)


# # [DETECTION DATA] - Create
@login_required
def detection_create(request, monitoring_id):
    """
    + Function: detection_create
        - Description: allows the user to create the detection data through
                    a concatenated form, accessed on the previews page and
                    embedded in a modal. The data to insert is:
                        - Taxonomy
                        - Life history behavior
                        - Life history health condition
        + Sub-functions:
            - save(): creates/saves an object from the data bound to the form
            - render: returns an httpresponse in which the request data, 
                    template and data dictionary are found
        + Parameters:
            - request: requested data such as user data, session etc.
            - monitoring_id: monitoring id provides access to the existing
                    monitoring data that the form will use to create a 
                    related detection object
        + Variables:
            - animal_[name, class, order, family, genus, species, origin,
                    behavior, health]: ids obtained through the form boxes, 
                    which allow associating an object of the "animal kingdom" 
                    model to each attribute of the detected animal object
            - animal_detail: instance of a DetectedAnimal()
            + data: [dictionary] data delivered to front-end
                - monitoring_id: Monitoring id of the object to update
                - response: response from the attempt to update monitoring data                
            - template_name: path to the template that will use the data inser-
                    ted in the dictionary named 'data'
    """
    data = {}

    # Add monitoring id to dictionary
    data['monitoring_id']=monitoring_id

    # Try to save data if the method = Post
    if request.method == 'POST':
        request_getdata = request.POST
        
        # Set default data or form data
        for form_data in request_getdata:
            fd = request_getdata[form_data]
            if fd == 'No identificado' or fd =='' or fd =='Sin información':
                if form_data =='animal_name':
                    animal_name = '1'
                elif form_data == 'animal_class':
                    animal_class = '1'
                elif form_data == 'animal_order':
                    animal_order = '1'
                elif form_data == 'animal_family':
                    animal_family = '1'
                elif form_data == 'animal_genus':
                    animal_genus = '1'
                elif form_data == 'animal_species':
                    animal_species = '1'
                elif form_data == 'animal_origin':
                    animal_origin = '1'
                elif form_data == 'lifehistory_behavior':
                    behavior = 'Sin información'
                elif form_data == 'lifehistory_health_condition':
                    health = 'Sin información'
                elif form_data == 'notes':
                    notes = ''
                
            elif fd != 'No identificado' or fd !='' or fd !='Sin información':
                if form_data =='animal_name':
                    animal_name = request_getdata['animal_name']
                elif form_data == 'animal_class':
                    animal_class = request_getdata['animal_class']
                elif form_data == 'animal_order':
                    animal_order = request_getdata['animal_order']
                elif form_data == 'animal_family':
                    animal_family = request_getdata['animal_family']
                elif form_data == 'animal_genus':
                    animal_genus = request_getdata['animal_genus']
                elif form_data == 'animal_species':
                    animal_species = request_getdata['animal_species']
                elif form_data == 'animal_origin':
                    animal_origin = request_getdata['animal_origin']
                elif form_data == 'lifehistory_behavior':
                    behavior = request_getdata['lifehistory_behavior']
                elif form_data == 'lifehistory_health_condition':
                    health = request_getdata['lifehistory_health_condition']
                elif form_data == 'notes':
                    notes = request_getdata['notes']
        
        #Insert data to Database
        animal_detail = DetectedAnimal()
        animal_detail.monitoring = Monitoring.objects.get(id=monitoring_id)
        animal_detail.animal = Animal.objects.get(id=1)
        animal_detail.animal_name = AnimalName.objects.get(id=animal_name)
        animal_detail.animal_class = AnimalClass.objects.get(id=animal_class)
        animal_detail.animal_order = AnimalOrder.objects.get(id=animal_order)
        animal_detail.animal_family = AnimalFamily.objects.get(id=animal_family)
        animal_detail.animal_genus = AnimalGenus.objects.get(id=animal_genus)
        animal_detail.animal_species = AnimalSpecies.objects.get(id=animal_species)
        animal_detail.animal_origin = AnimalOrigin.objects.get(id=animal_origin)
        animal_detail.lifehistory_behavior = behavior
        animal_detail.lifehistory_health_condition = health
        animal_detail.notes = notes

        # Try to send data to database and return a message
        try:
            animal_detail.save()
            ajax_response = ("\nRegistro de monitoreo: " + str(monitoring_id) +
                        "\nLos datos ingresados por el usuarios han sido "+
                        "guardados en la base de datos de detecciones\n"+
                        "\nPuede cerrar este mensaje para seguir ingresando "+
                        "nuevos registros\n")
            return JsonResponse({'response':ajax_response})
        except:
            ajax_response = ("Atención!\n\n" +
                        "\nRegistro de monitoreo: " + str(monitoring_id) +
                        '\n\nError, no se han guardado los datos ingresados')
            return JsonResponse({'response':ajax_response})
            
          
    #Send data to front-end (modal)
    template_name = '../templates/client_detection_create.html'
    return render(request, template_name, data)


# TODO: [DETECTION DATA] - Update --> next feature
# TODO: [DETECTION DATA] - Delete --> next feature


# --------------------  [CONCATENATED FORM FUNCTIONS] --------------------
def get_animal(request):
    """
    + Function get_animal
        - Description: gets the existing AnimalName objects and appends the
                    id and name to a json response.
        + Sub-functions:
            - append: add data to a list array
            - JsonResponse: returns a json to front-end
        + Variables:
            - animal_names = Fetch AnimalName objects
            - list_name: id,name array
    """
    
    # Filter data
    animal_names = AnimalName.objects.all()

    # Create id,name list of animal names
    list_name = []
    for names in animal_names:
        list_name.append({
                    'id':names.id,
                    'name':names.name,
                }
            )

    # Returns data to de modal detection_create
    return JsonResponse({'list_name':list_name})


def get_animal_class(request):
    """
    + Function get_animal_class
        - Description: gets the existing AnimalClass objects related to the
                AnimalName id selected by the user, and appends the id and 
                scientific name to a json response.
        + Sub-functions:
            - filter: filter the animal classes related to animal name id
            - append: add data to a list array
            - JsonResponse: returns a json to front-end
        + Variables:
            - animal_classes = Filtered AnimalClass objects
            - list_name: id,scientific_name array
    """

    # Filter data
    animal_classes = AnimalClass.objects.filter(animal_name__id=request.GET['aname'])

    # Create id,scientific_name list of animal classes
    list_classes = []
    for animal in animal_classes:
        list_classes.append({
                    'id':animal.id,
                    'scientific_name':animal.scientific_name,
                }
            )
    
    # Returns data to de modal detection_create
    return JsonResponse({'list_classes':list_classes})


def get_animal_order(request):
    """
    + Function get_animal_order
        - Description: gets the existing AnimalOrder objects related to the
                AnimalClass id selected by the user, and appends the id and 
                scientific name to a json response.
        + Sub-functions:
            - filter: filter the animal orders related to animal class id
            - append: add data to a list array
            - JsonResponse: returns a json to front-end
        + Variables:
            - animal_orders = Filtered AnimalOrder objects
            - list_orders: id,scientific_name array
    """
    
    # Filter data
    animal_orders = AnimalOrder.objects.filter(animal_class__id=request.GET['aclass'])

    # Create id,scientific_name list of animal orders
    list_orders = []
    for order in animal_orders:
        list_orders.append({
                    'id':order.id,
                    'scientific_name':order.scientific_name,
                }
            )
        
    # Returns data to de modal detection_create
    return JsonResponse({'list_orders':list_orders})


def get_animal_family(request):
    """
    + Function get_animal_family
        - Description: gets the existing AnimalFamily objects related to the
                AnimalOrder id selected by the user, and appends the id and 
                scientific name to a json response.
        + Sub-functions:
            - filter: filter the animal orders related to animal order id
            - append: add data to a list array
            - JsonResponse: returns a json to front-end
        + Variables:
            - animal_families = Filtered AnimalFamily objects
            - list_families: id,scientific_name array
    """

    # Filter data
    animal_families = AnimalFamily.objects.filter(animal_order__id=request.GET['aorder'])

    # Create id,scientific_name list of animal families
    list_families = []
    for family in animal_families:
        list_families.append({
                    'id':family.id,
                    'scientific_name':family.scientific_name,
                }
            )
        
    # Returns data to de modal detection_create
    return JsonResponse({'list_families':list_families})


def get_animal_genus(request):
    """
    + Function get_animal_genus
        - Description: gets the existing AnimalGenus objects related to the
                AnimalFamily id selected by the user, and appends the id and 
                scientific name to a json response.
        + Sub-functions:
            - filter: filter the animal orders related to animal Family id
            - append: add data to a list array
            - JsonResponse: returns a json to front-end
        + Variables:
            - animal_genuses = Filtered AnimalGenus objects
            - list_genuses: id,scientific_name array
    """

    # Filter data
    animal_genuses = AnimalGenus.objects.filter(animal_family__id=request.GET['afamily'])

    # Create id,scientific_name list of animal genuses
    list_genuses = []
    for genus in animal_genuses:
        list_genuses.append({
                    'id':genus.id,
                    'scientific_name':genus.scientific_name,
                }
            )
        
    # Returns data to de modal detection_create
    return JsonResponse({'list_genuses':list_genuses})


def get_animal_species(request):
    """
    + Function get_animal_species
        - Description: gets the existing AnimalSpecies objects related to the
                AnimalGenus id selected by the user, and appends the id and 
                scientific name to a json response.
        + Sub-functions:
            - filter: filter the animal orders related to animal Genus id
            - append: add data to a list array
            - JsonResponse: returns a json to front-end
        + Variables:
            - animal_species = Filtered AnimalSpecies objects
            - list_species: id,scientific_name array
    """
    
    # Filter data
    animal_species = AnimalSpecies.objects.filter(animal_genus__id=request.GET['agenus'])

    # Create id,scientific_name list of animal species
    list_species = []
    for species in animal_species:
        list_species.append({
                    'id':species.id,
                    'scientific_name':species.scientific_name,
                }
            )
        
    # Returns data to de modal detection_create
    return JsonResponse({'list_species':list_species})


def get_animal_origin(request):
    """
    + Function get_animal_origin
        - Description: gets the existing AnimalOrigin objects related to the
                AnimalSpecies id selected by the user, and appends the id and 
                scientific name to a json response.
        + Sub-functions:
            - filter: filter the animal orders related to animal species id
            - append: add data to a list array
            - JsonResponse: returns a json to front-end
        + Variables:
            - animal_origins = Filtered AnimalOrigin objects
            - list_origins: id,scientific_name array
    """
    
    # Filter data
    animal_origins = AnimalOrigin.objects.filter(animal_species__id=request.GET['aspecies'])

    # Create id,common_name list of animal origins
    list_origins = []
    for origin in animal_origins:
        list_origins.append({
                    'id':origin.id,
                    'common_name':origin.common_name,
                }
            )
        
    # Returns data to de modal detection_create
    return JsonResponse({'list_origins':list_origins})



# --------------- END SAVE TAXONOMY AND LIFE HISTORY OF THE ANIMAL ---------------

@login_required
def monitoring_delete(request):
    data = {}
    # Get the data related to the logged user
    data['user'], stations,  data['organization'], data['ustations'] = session_data(request.user)
    # Get the monitoring data  where record = True
    data['monitoring'] = Monitoring.objects.filter(station = stations.id)

    template_name = '../templates/client_monitoring_delete.html'
    return render(request, template_name, data)


@login_required
def get_csv(request, station_id, getcsv=0):
    """
    + Function: get_csv
        - Description: Get csv of detected animals, and allows the user to
                    download the file.
        + Parameters:
            - station_id: selected station to show data (used by the other 
                        functionalities)
            - getcsv: if the value = 0 --> get the base page, if value = 1, 
                    download the csv
        + Variables:
            - detections: list of the 'detection animals' objects
            - fields: fields of the 'DetectedAnimal' model, and the FK fields
                    related, that will be used to fill the data table.
            - timestamp: now datetime
            - response: Http response for csv content
            - writer: csv object, created from the given fields and header
            - data: [dictionary] data delivered to front-end
                - logged_user
                - logged_station
                - organization_name
                - user_stations
                - selected_station
                - station_name
                - detections:[object] list of the 'detection animals' objects
            - template_name: path to the template that will use the data inser-
                    ted in the dictionary named 'data'
    """
    
    data = {}
    
    # Get the data related to the logged user
    data = session_data(request.user, station_id)

    # Get the data from detected animals
    detections = DetectedAnimal.objects.all()
    data['detections'] = detections

    # download csv of detections
    if getcsv==1:
        
        # Populate the list with models fields (__ to get the parent atribute)
        fields = [
            'id',
            'monitoring__id',
            'monitoring__station__id',
            'monitoring__station__fauno_project__name',
            'monitoring__station__fauno_project__organization__name',
            'monitoring__monitoring_datetime',
            'monitoring__record',
            'monitoring__false_positive',
            'monitoring__elements_counter',
            'monitoring__human_presence',
            'monitoring__status',
            'monitoring__image_url',
            'monitoring__video_url',
            'monitoring__in_gallery',
            'monitoring__notes',
            'animal__id',
            'animal__kingdom',
            'animal__phylum',
            'animal_name__id',
            'animal_name__name',
            'animal_class__id',
            'animal_class__scientific_name',
            'animal_class__common_name',
            'animal_order__id',
            'animal_order__scientific_name',
            'animal_order__common_name',
            'animal_family__id',
            'animal_family__scientific_name',
            'animal_genus__id',
            'animal_genus__scientific_name',
            'animal_species__id',
            'animal_species__scientific_name',
            'animal_species__common_name',
            'animal_origin__id',
            'animal_origin__common_name',
            'lifehistory_behavior',
            'lifehistory_health_condition',
            'notes'
        ]

        # Generate the csv file
        timestamp = datetime.now().isoformat()
        response = HttpResponse(content_type="text/csv")
        response[
            "Content-Disposition"
        ] = f"attachment; filename=detected_animals_{timestamp}.csv"
        writer = csv.writer(response)
        # Write the header row
        writer.writerow(fields)
        # Use the fields to get the data
        for row in DetectedAnimal.objects.values(*fields):
            writer.writerow([row[field] for field in fields])
        # return
        return response

    # Send data to front-end
    template_name = '../templates/manager_getdata.html'
    return render(request, template_name, data)


