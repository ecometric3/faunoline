from django.db import models
from django.core import validators
# External models import
from django.contrib.auth.models import User
from manager.models import FaunoProject
from animalkingdom.models import(
        Animal, AnimalName,
        AnimalClass, AnimalOrder,
        AnimalFamily, AnimalGenus,
        AnimalSpecies, AnimalOrigin
)

from client.utils import definitions, validators


class Station(models.Model):

    """
    + Clase Station
        - Descripción: Identifica la estacion asociada al proyecto Fauno
        - Relación: FaunoProject [1-n] Station
        - Atributos:
            - fauno_project : Foreign key - objeto fauno_project asociado
            - longitude: parámetro  de coordenada geografica de la estacion
            - latitude: parámetro  de coordenada geografica de la estacion
            - habitat: habitat del entorno donde se instalo la estacion
                - native_forest
                - meadow (pradera?)
                - plantation
                - scrubland (matorrales?)
            - target: qué especies se espera encontrar en la zona donde se 
                    ubica la estación
            - status: si la estación está activa o inactiva
                - active
                - inactive
    """
    # Fk
    fauno_project = models.ForeignKey(FaunoProject, on_delete = models.CASCADE)
    # Atributos
    latitude = models.FloatField()
    longitude = models.FloatField()
    habitat = models.CharField(
            max_length=20,
            choices=definitions.habitats,
            default="native_forest")
    target = models.CharField(
            max_length=20,
            default=None,
            validators= validators.validator_target_length)
    status = models.CharField(
            max_length=20,
            choices=definitions.station_status,
            default="inactive")

    def __str__(self):
        return '(%s - %s)(%s)' % (self.fauno_project.name,
                                self.fauno_project.pk,
                                self.pk)

    class Meta:
        verbose_name_plural = "Stations"


class UserXStation(models.Model):

    """
    + Clase UserXFaunoProject
        - Descripcion: clase intermedia de relación [n:n] entre usuario y 
                        proyectos Fauno
        - Relación: User, Station [1:n] UserXStation
        - Atributos
            - user: Foreign key - objeto user asociado
            - fauno_project: Foreign key - objeto fauno_project asociado
    """
    # Fk
    user  = models.ForeignKey(
            User,
            on_delete = models.CASCADE)
    station  = models.ForeignKey(
            Station,
            on_delete = models.CASCADE)

    def __str__(self):
        return '%s - %s (%s)' % (self.user.username, self.station.fauno_project.name,self.pk)

    class Meta:
        verbose_name_plural = "Station Users"


class Hardware(models.Model):
    
    """
    + Clase Hardware
        - Descripcion: Partes que componen la estacion: raspberry, sd, camara,
        - Relacion: Station [1-n] Hardware
        - Atributos:
            - station: Foreign key - objeto station asociado
            - hardware_type: raspberry, sd-card, camera, etc
            - status: working, on revision, disabled
            - model: modelo del equipo
            - last_install_date: última vez que este equipo fue instalado en la
                                estacion ya sea porque es nuevo, fue reparado o
                                actualizado.
            - last_uninstall_date: última vez que este equipo fue desinstalado 
                                de la estacion ya sea porque debe ser reparado,
                                actualizado o debe instalarse en otra estación 
                                Fauno.
            - usage_counter: contador con la cantidad de veces que el hardware 
                            ha sido instalado en diferentes estaciones, o en la
                            misma si éste fue reparado o actualizado.
    """

    # Fk
    station = models.ForeignKey(Station, on_delete = models.CASCADE)
    # Atributos
    hardware_type  = models.CharField(
            max_length = 20,
            choices = definitions.hardware_types, 
            default="raspberry")
    status = models.CharField(
            max_length = 15,
            choices = definitions.hardware_status,
            default="working") 
    model = models.CharField(
            max_length=45,
            default=None,
            validators=validators.validator_model_length)
    last_install_date = models.DateField(
            auto_now=False,
            blank=True,
            null=True)
    last_uninstall_date = models.DateField(
            auto_now=False,
            blank=True,
            null=True)
    usage_counter = models.IntegerField(default=0)

    def __str__(self):
        return '(%s) (%s - %s)' % (self.hardware_type,
                                self.station.fauno_project.name,
                                self.station.fauno_project.pk)

    class Meta:
        verbose_name_plural = "Hardwares"


class HardwareLog(models.Model):
    
    """
    + Clase HardwareLog
        - Descripcion: Log con el historial de uso del equipo
        - Relacion: Hardware [1:n] HardwareLog
        - Atributos:
            - hardware: Foreign key - objeto hardware asociado
            - log_type: tipos de log:
                - maintenance
                - failure_revision
                - upgrade
                - replacement
            - log_datetime: fecha
            - log_detail: descripción
    """

    # Fk
    hardware = models.ForeignKey(Hardware, on_delete = models.CASCADE)
    # Atriutos
    log_type = models.CharField(
            max_length = 20,
            choices = definitions.log_types,
            default="maintenance")
    log_datetime = models.DateField(auto_now=False)
    log_datail = models.CharField(
            max_length=100,
            default=None,
            validators= validators.validator_log_length)

    def __str__(self):
        return '(%s - %s - %s) (%s)' % (
                self.hardware.hardware_type,
                self.hardware.station.fauno_project.name,
                self.hardware.station.fauno_project.pk,
                self.pk)

    class Meta:
        verbose_name_plural = "Hardware Logs"


class Monitoring(models.Model):
    
    """
    + Clase Monitoring
        - Descripcion: Almacena los datos de las notificaciones de la estación
                     (status/monitoreo )
        - Relacion: Station [0:n] Monitoring
        - Atributos:
            - station: Foreign key - objeto station asociado
            - monitoring_datetime: fecha:hr
            - record: boolean - (old version: animal_alert)
                - True: es un registro por movimiento
                - False: es monitoreo de funcionamiento
            - false_positive(*): boolean
                - True: registro de movimiento sin elementos detectados
                - False: registro de movimiento con elementos detectados
            - elements_counter(*)(**): cantidad de elementos en la imagen/video
            - human_presence(*)(**): boolean
                - True: registro de movimiento con humanos detectados
                - False: registro de movimiento sin humanos detectados
            - status(*): estado de revisión del monitoreo
                - viewed: visto por el usuario
                - pending_monitoring: falta actualizar datos de Monitoreo
                - pending_taxonomy: falta actualizar datos de Taxonomia
                - reviewed: Ya fue revisado
            - image_link: link de la imagen en s3
            - video_link: link del video en s3
            - in_gallery: boolean
                - True: Se visualizará desde la galeria
                - False: No se visualizará desde la galeria
            - notes(*): apuntes de lo observado en img/video capturado

        (*) Datos ingresados por el usuario. 
        (**) Datos que en  futuras etapas seran actualizadas con ML
    """

    # Fk
    station = models.ForeignKey(Station, on_delete = models.CASCADE)
    #Atributos
    monitoring_datetime = models.DateTimeField(auto_now=False)
    record = models.BooleanField(default=False) # old version: animal_alert
    false_positive = models.BooleanField(blank=True, default=True)
    elements_counter = models.IntegerField(blank=True, null=True)
    human_presence = models.BooleanField(blank=True, default=False)
    status = models.CharField(
        max_length = 20,
        choices = definitions.monitoring_status,
        blank=True,
        default="viewed")
    image_url = models.CharField(max_length = 150,blank=True, default="")
    video_url = models.CharField(max_length = 150,blank=True, default="")
    in_gallery = models.BooleanField(blank=True, default=False)
    notes = models.TextField(blank=True, null=True)

    def __str__(self):
        return ' (%s - %s) (%s)' % (
                self.station.fauno_project.name,
                self.station.fauno_project.pk,
                self.pk)
        
    class Meta:
        verbose_name_plural = "Monitoring"


# NEW MODEL FOR TAXONOMY DATA
class DetectedAnimal(models.Model):
    #pk auto id
    # blank=True, null=True
    monitoring = models.ForeignKey(Monitoring, on_delete = models.CASCADE,
                                blank=True, null=True,default=1)
    animal = models.ForeignKey(Animal, on_delete = models.CASCADE,
                                blank=True, null=True,default=1)
    animal_name = models.ForeignKey(AnimalName, on_delete = models.CASCADE,
                                blank=True, null=True,default=1)
    animal_class = models.ForeignKey(AnimalClass, on_delete = models.CASCADE,
                                blank=True, null=True,default=1)
    animal_order = models.ForeignKey(AnimalOrder, on_delete = models.CASCADE,
                                blank=True, null=True,default=1)
    animal_family = models.ForeignKey(AnimalFamily, on_delete = models.CASCADE,
                                blank=True, null=True,default=1)
    animal_genus = models.ForeignKey(AnimalGenus, on_delete = models.CASCADE,
                                blank=True, null=True,default=1)
    animal_species = models.ForeignKey(AnimalSpecies, on_delete = models.CASCADE,
                                blank=True, null=True,default=1)
    animal_origin = models.ForeignKey(AnimalOrigin, on_delete = models.CASCADE,
                                blank=True, null=True,default=1)
    
    lifehistory_behavior = models.CharField(
            max_length = 20,
            #choices = definitions.lifehis_behaviors,
            default="Sin información")
    lifehistory_health_condition = models.CharField(
            max_length = 20,
            #choices = definitions.lifehis_health_condition,
            default="Sin información")
    notes = models.TextField(blank=True, null=True)

    def __str__(self):
        # Model id M: Monitoring id A: animal name
        return '(id:%s)(M:%s A:%s) ' % (
                self.pk,
                self.monitoring.pk,
                self.animal_name.name
                )
        
    class Meta:
        verbose_name_plural = "Detected animal"
        

#TO DELETE:
class DetectedElement(models.Model):

    """
    + Clase DetectedElement
        - Descripcion: Asocia a un registro de Monitoring la taxonomía y
                    la historia de vida de los elementos detectados
        - Relacion:  Monitoring [0:n] DetectedElement
        - Atributos:
            - monitoring: Foreign key - objeto monitoring asociado
            - notes: apuntes de lo observado en el elemento detectado
    """
    # Fk
    monitoring = models.ForeignKey(Monitoring, on_delete = models.CASCADE)
    # Atributos
    taxonomy_kingdom = models.CharField(
            max_length = 15,
            choices = definitions.txnmy_kingdoms,
            default="animalia")
    taxonomy_phyllum = models.CharField(
            max_length = 15,
            choices = definitions.txnmy_phyllums,
            default="chordata")
    taxonomy_class = models.CharField(
            max_length = 15,
            choices = definitions.txnmy_classes,
            default="unidentified")
    taxonomy_common_class = models.CharField(
            max_length = 15,
            choices = definitions.txnmy_common_classes,
            default="unidentified")      
    taxonomy_order = models.CharField(
            max_length = 20,
            choices = definitions.txnmy_orders,
            default="unidentified")
    taxonomy_common_order = models.CharField(
            max_length = 20,
            choices = definitions.txnmy_common_orders,
            default="unidentified")
    taxonomy_family = models.CharField(
            max_length = 20,
            choices = definitions.txnmy_families,
            default="unidentified")
    taxonomy_genus = models.CharField(
            max_length = 20,
            choices = definitions.txnmy_genuses,
            default="unidentified")
    taxonomy_species = models.CharField(
            max_length = 30,
            choices = definitions.txnmy_species,
            default="unidentified")
    taxonomy_common_species = models.CharField(
            max_length = 30,
            choices = definitions.txnmy_common_species,
            default="unidentified")
    taxonomy_origin = models.CharField(
            max_length = 15,
            choices = definitions.txnmy_origin,
            default="unidentified")
    lifehistory_behavior = models.CharField(
            max_length = 20,
            choices = definitions.lifehis_behaviors,
            default="unidentified")
    lifehistory_health_condition = models.CharField(
            max_length = 20,
            choices = definitions.lifehis_health_condition,
            default="unidentified")
    notes = models.TextField(blank=True, null=True)

    def __str__(self):
        return ' (%s ) (%s) (%s)' % (
                self.monitoring.station.fauno_project.name,
                self.monitoring,
                self.pk)

    class Meta:
        verbose_name_plural = "Detected Elements"