from csvexport.actions import csvexport

from django.contrib import admin

from client.models import (
    DetectedElement,
    Station, 
    Hardware, 
    HardwareLog, 
    Monitoring,
    UserXStation,
    DetectedAnimal)


@admin.register(Station)
class StationAdmin(admin.ModelAdmin):
    pass


@admin.register(Hardware)
class HardwareAdmin(admin.ModelAdmin):
    pass


@admin.register(HardwareLog)
class HardwareLogAdmin(admin.ModelAdmin):
    pass


@admin.register(Monitoring)
class MonitoringAdmin(admin.ModelAdmin):

    list_display = [
        'id',
        'station',
        'monitoring_datetime',
        'record',
        'false_positive',
        'elements_counter',
        'human_presence',
        'status',
        'image_url',
        'video_url',
        'in_gallery',
        'notes'
    ]

    csvexport_export_fields = [
        'id',
        'station.id',
        'station.fauno_project.name',
        'station.fauno_project.organization.name',
        'monitoring_datetime',
        'record',
        'false_positive',
        'elements_counter',
        'human_presence',
        'status',
        'image_url',
        'video_url',
        'in_gallery',
        'notes',
    ]

    csvexport_selected_fields = [
        'id',
        'station.id',
        'station.fauno_project.name',
        'monitoring_datetime',
        'record',
        'false_positive',
        'elements_counter',
        'human_presence',
        'status',
        'image_url',
        'video_url',
        'in_gallery',
        'notes',
    ]
    actions = [csvexport]



@admin.register(UserXStation)
class UserXStation(admin.ModelAdmin):
    pass

@admin.register(DetectedElement)
class DetectedElementAdmin(admin.ModelAdmin):
    pass

@admin.register(DetectedAnimal)
class DetectedAnimalAdmin(admin.ModelAdmin):

    list_display = [
        'id',
        'monitoring',
        'animal',
        'animal_name',
        'animal_class',
        'animal_order',
        'animal_family',
        'animal_genus',
        'animal_species',
        'animal_origin',
        'lifehistory_behavior',
        'lifehistory_health_condition',
        'notes'
    ]

    csvexport_export_fields = [
        'id',
        'monitoring.id',
        'monitoring.station.id',
        'monitoring.station.fauno_project.name',
        'monitoring.station.fauno_project.organization.name',
        'monitoring.monitoring_datetime',
        'monitoring.record',
        'monitoring.false_positive',
        'monitoring.elements_counter',
        'monitoring.human_presence',
        'monitoring.status',
        'monitoring.image_url',
        'monitoring.video_url',
        'monitoring.in_gallery',
        'monitoring.notes',
        'animal.id',
        'animal.kingdom',
        'animal.phylum',
        'animal_name.id',
        'animal_name.name',
        'animal_class.id',
        'animal_class.scientific_name',
        'animal_class.common_name',
        'animal_order.id',
        'animal_order.scientific_name',
        'animal_order.common_name',
        'animal_family.id',
        'animal_family.scientific_name',
        'animal_genus.id',
        'animal_genus.scientific_name',
        'animal_species.id',
        'animal_species.scientific_name',
        'animal_species.common_name',
        'animal_origin.id',
        'animal_origin.common_name',
        'lifehistory_behavior',
        'lifehistory_health_condition',
        'notes'
    ]

    csvexport_selected_fields = [
        'id',
        'monitoring.id',
        'monitoring.station.id',
        'monitoring.station.fauno_project.name',
        'monitoring.station.fauno_project.organization.name',
        'monitoring.monitoring_datetime',
        'monitoring.record',
        'monitoring.false_positive',
        'monitoring.elements_counter',
        'monitoring.human_presence',
        'monitoring.status',
        'monitoring.image_url',
        'monitoring.video_url',
        'monitoring.in_gallery',
        'monitoring.notes',
        'animal.id',
        'animal.kingdom',
        'animal.phylum',
        'animal_name.id',
        'animal_name.name',
        'animal_class.id',
        'animal_class.scientific_name',
        'animal_class.common_name',
        'animal_order.id',
        'animal_order.scientific_name',
        'animal_order.common_name',
        'animal_family.id',
        'animal_family.scientific_name',
        'animal_genus.id',
        'animal_genus.scientific_name',
        'animal_species.id',
        'animal_species.scientific_name',
        'animal_species.common_name',
        'animal_origin.id',
        'animal_origin.common_name',
        'lifehistory_behavior',
        'lifehistory_health_condition',
        'notes'
    ]
    actions = [csvexport]