from django.urls import path
from client import views
from client import api
#from auth_login import views

app_name = 'client'

urlpatterns = [
    # [Módulo base]
    path('home_base',
        views.home_client_base,
        name='home_base'),

    # [Módulo Notificaciones]
    path('notificaciones/<int:station_id>',
        views.notifications,
        name='notifications'),

    # [Modulo galeria]
    path('galeria/<int:station_id>',
        views.gallery,
        name='gallery'),
    # [Modulo galeria] - detail
    path('galeria/detalle/<int:station_id>/<int:gallery_id>',
        views.gallery_detail,
        name='gallery_detail'),

    # [Modulo Estación]
    path('estacion/<int:station_id>',
        views.station_data,
        name='station'),

    # [Modulo Taxonomía] - [monitoring records] - processing
    path('taxon/procesamiento/<int:station_id>',
        views.monitoring_processing,
        name='monitoring_processing'),
    # [Modulo Taxonomía] - [monitoring records] - update (modal in proccesing)
    path('taxon/procesamiento/actualizar_registro/<int:monitoring_id>',
        views.monitoring_update,
        name='monitoring_update'),
    # TODO: [Modulo Taxonomía] - [monitoring records] - list --> new feature
    # TODO: [Modulo Taxonomía] - [monitoring records] - delete --> new feature

    # [Modulo Taxonomía] - [detection records] - list
    path('taxon/lista_detecciones/<int:station_id>',
        views.detection_list,
        name='detection_list'),
    # [Modulo Taxonomía] - [detection records] - create
    path('taxon/procesamiento/crear_deteccion/<int:monitoring_id>',
        views.detection_create,
        name='detection_create'),
    # TODO: [Modulo Taxonomía] - [detection records] - update --> new feature
    # TODO: [Modulo Taxonomía] - [detection records] - delete --> new feature
    # Funciones Get para cargar formulario concatenado
    path('get/animals/', views.get_animal, name='get_animal'),
    path('get/animalsclass/', views.get_animal_class, name='get_animal_class'),
    path('get/animalsorder/', views.get_animal_order, name='get_animal_order'),
    path('get/animalsfamily/', views.get_animal_family, name='get_animal_family'),
    path('get/animalsgenus/', views.get_animal_genus, name='get_animal_genus'),
    path('get/animalsspecies/', views.get_animal_species, name='get_animal_species'),
    path('get/animalsorigin/', views.get_animal_origin, name='get_animal_origin'),

    # [Get and Post data] - API
    path('api/home', api.HomeClientAPIView.as_view(), 
        name='home_api'),
    # [Get and Post data] - Detections csv
    path('gestion/csv/<int:station_id>/<int:getcsv>',views.get_csv, name='get_csv'),
    
]
