from django import forms
from client.models import Monitoring, DetectedElement
  
  
# Formulario de actualización de monitoreo
class MonitoringForm(forms.ModelForm):
  
    # create meta class
    class Meta:
        # Model to be used
        model = Monitoring

        labels = {
            'id':'ID',
            'record':'Registro',
            'false_positive':'Falso positivo',
            'elements_counter':'N° Elementos',
            'human_presence':'Presencia Humana',
            'in_gallery':'Galería',
            'status':'Estado de revisión',
            'notes':'Notas'
        }
  
        # Fields to be used
        fields = [
            "id",
            "record",
            "false_positive",
            "elements_counter",
            "human_presence",
            "in_gallery",
            "status",
            "notes"
        ]

# Formulario de actualización de taxoinomía
class DetectedElementForm(forms.ModelForm):
  
    def __init__(self, *args, **kwargs):
        super(DetectedElementForm, self).__init__(*args, **kwargs)
        
        self.fields['monitoring'].widget = forms.HiddenInput()


    # create meta class
    class Meta:
        # Model to be used
        model = DetectedElement

        labels = {
            'taxonomy_kingdom':'Reino',
            'taxonomy_phyllum':'Phyllum',
            'taxonomy_class':'Clase',
            'taxonomy_common_class':'Clase(común)',
            'taxonomy_order':'Orden',
            'taxonomy_common_order':'Orden(común)',
            'taxonomy_family':'Familia',
            'taxonomy_genus':'Género',
            'taxonomy_species':'Especie',
            'taxonomy_common_species':'Especie(común)',
            'taxonomy_origin':'Origen',
            'lifehistory_behavior':'Comportamiento',
            'lifehistory_health_condition':'Condición de salud',
            'notes':'Observaciones'
        }


        # help_texts = {
        #     'taxonomy_kingdom': 'Some useful help text.',
        # }


        # Fields to be used
        fields = '__all__'  
        # fields = [

        #     "id",
        #     "monitoring",
        #     "taxonomy_kingdom",
        #     "taxonomy_phyllum",
        #     "taxonomy_class",
        #     "taxonomy_common_class",
        #     "taxonomy_order",
        #     "taxonomy_common_order",
        #     "taxonomy_family",
        #     "taxonomy_genus",
        #     "taxonomy_species",
        #     "taxonomy_common_species",
        #     "taxonomy_origin",
        #     "lifehistory_behavior", 
        #     "lifehistory_health_condition",
        #     "notes"
        # ]
        

