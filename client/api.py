from rest_framework.response import Response
from rest_framework import (
    generics,
    status,
    permissions
)

from client.utils import serializers


class HomeClientAPIView(generics.GenericAPIView):
    
    """
    + Clase HomeClientAPIView
        - Descripción: api recepción de datos de Raspberry Katalapi
        + Atributos:
            serializers_class: clase que verifica que lo recibido es correcto.
    """
    serializer_class = serializers.HomeClientAPIViewSerializer

    def get(self, request):

        data = {}
        data['ecometric'] = "ok "
        
        return Response(data, status=status.HTTP_200_OK)

    def post(self, request):
        data = {}

        serializer = self.serializer_class(
            data=request.data,
            context=request
        )
        
        if serializer.is_valid():
            data['msg'] = "Success"
            data['station_id'] = serializer.validated_data['station_id']
            data['record'] = serializer.validated_data['record']
            data['image_url'] = serializer.validated_data['image_url']
            data['video_url'] = serializer.validated_data['video_url']
            data['monitoring_datetime'] = serializer.validated_data['monitoring_datetime']

            serializer.create(serializer.validated_data)
            status_code = status.HTTP_200_OK

        else:
            data['msg'] = serializer.errors
            status_code = status.HTTP_400_BAD_REQUEST

        return Response(
            data,
            status=status_code
        )
