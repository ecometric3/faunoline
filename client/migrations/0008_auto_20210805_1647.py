# Generated by Django 3.2.6 on 2021-08-05 16:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0007_auto_20210713_2221'),
    ]

    operations = [
        migrations.AlterField(
            model_name='detectedanimal',
            name='lifehistory_behavior',
            field=models.CharField(choices=[('sin_informacion', 'Sin información'), ('hunting', 'Cazando'), ('passing_through', 'De paso'), ('feeding', 'Alimentándose')], default='unidentified', max_length=20),
        ),
        migrations.AlterField(
            model_name='detectedanimal',
            name='lifehistory_health_condition',
            field=models.CharField(choices=[('sin_informacion', 'Sin información'), ('healthy', 'Sano'), ('ill', 'Enfermo')], default='unidentified', max_length=20),
        ),
        migrations.AlterField(
            model_name='detectedelement',
            name='lifehistory_behavior',
            field=models.CharField(choices=[('sin_informacion', 'Sin información'), ('hunting', 'Cazando'), ('passing_through', 'De paso'), ('feeding', 'Alimentándose')], default='unidentified', max_length=20),
        ),
        migrations.AlterField(
            model_name='detectedelement',
            name='lifehistory_health_condition',
            field=models.CharField(choices=[('sin_informacion', 'Sin información'), ('healthy', 'Sano'), ('ill', 'Enfermo')], default='unidentified', max_length=20),
        ),
    ]
