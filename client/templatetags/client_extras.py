from django import template

register = template.Library()

def comma_to_dot(value):
    return str(value)
    # return "{}".format(value)

register.filter('comma_to_dot', comma_to_dot)