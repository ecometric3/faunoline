from django.contrib import admin
from manager.models import (
        EcometricProject,
        ProjectLog,
        Organization,
        FaunoProject)

@admin.register(EcometricProject)
class EcometricProjectAdmin(admin.ModelAdmin):
    pass


@admin.register(ProjectLog)
class ProjectLogAdmin(admin.ModelAdmin):
    pass


@admin.register(Organization)
class OrganizationAdmin(admin.ModelAdmin):
    pass


@admin.register(FaunoProject)
class FaunoProjectAdmin(admin.ModelAdmin):
    pass
      