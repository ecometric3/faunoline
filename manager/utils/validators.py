from django.core import validators

"""
+ validators.py
    - Objetivo: validadores de objetos usados por los modelos
    + Validadores:
        - project_name: nombre del proyecto
        - manager_name: nombre del project manager
        - status_detail: detalle del estado del proyecto

"""

# [EcometricProject Model] - name validator
project_name_minmsg = "El nombre del projecto debe tener mínimo 3 caracteres"
project_name_maxmsg = "El nombre del projecto debe tener máximo 45 caracteres"

validator_project_name_length = [
    validators.MinLengthValidator(3, project_name_minmsg),
    validators.MaxLengthValidator(45, project_name_maxmsg)
]

# [EcometricProject Model] - Project manager name
manager_name_minmsg = "El nombre del Gestor debe tener mínimo 3 caracteres"
manager_name_maxmsg = "El nombre del Gestor debe tener máximo 45 caracteres"

validator_manager_name = [
    validators.MinLengthValidator(3,manager_name_minmsg),
    validators.MaxLengthValidator(45,manager_name_maxmsg)
]

# [EcometricProject Model] - [ProjectLog Model] -project status detail
status_detail_minmsg = "El detalle debe tener mínimo 10 caracteres"
status_detail_maxmsg = "El detalle debe tener máximo 100 caracteres"

validator_status_detail = [
    validators.MinLengthValidator(10,status_detail_minmsg),
    validators.MaxLengthValidator(100,status_detail_maxmsg)
]

# [Organization Model] - organization name
org_name_minmsg = "El nombre de la organización debe tener mínimo 3 caracteres"
org_name_maxmsg = "El nombre de la organización debe tener máximo 45 caracteres"

validator_org_name = [
    validators.MinLengthValidator(3,org_name_minmsg),
    validators.MaxLengthValidator(45,org_name_maxmsg)
]

# [Organization Model] - contact email
org_email_minmsg = "El email de contacto debe tener mínimo 3 caracteres"
org_email_maxmsg = "El email de contacto debe tener máximo 45 caracteres"

validators_org_email = [
    validators.MinLengthValidator(3,org_email_minmsg),
    validators.MaxLengthValidator(45,org_email_maxmsg)
]

# [Organization Model] - contact name
org_contact_minmsg = "El nombre del contacto debe tener mínimo 3 caracteres"
org_contact_maxmsg = "El nombre del contacto debe tener máximo 45 caracteres"

validators_org_contact = [
    validators.MinLengthValidator(3,org_contact_minmsg),
    validators.MaxLengthValidator(45,org_contact_maxmsg)
]

# [FaunoProject Model] - fauno project name
fauno_name_minmsg = "El nombre del proyecto Fauno debe tener mínimo 6 caracteres"
fauno_name_maxmsg = "El nombre del proyecto Fauno debe tener máximo 45 caracteres"

validators_fauno_name = [
    validators.MinLengthValidator(6,fauno_name_minmsg),
    validators.MaxLengthValidator(45,fauno_name_maxmsg)
]

# [FaunoProject Model] - fauno project description
description_minmsg = "La descripción del proyecto debe tener mínimo 3 caracteres"
description_maxmsg = "La descripción del proyecto debe tener máximo 45 caracteres"

validator_fauno_description = [
    validators.MinLengthValidator(10,description_minmsg),
    validators.MaxLengthValidator(100,description_maxmsg)
]
