"""
+ definitions.py
    - Objetivo: definiciones de objetos usados por los modelos de manager
    + Objetos:
        - Phases: fases del proyecto
        - Project status: estado del proyecto
"""

# [Project Model] - [ProjectLog Model] - Phases of the Project
project_phases = (
    ('initiation','Initiation'),
    ('planning','Planning'),
    ('developing','Developing'),
    ('in_pilot','In pilot'),
    ('execution','Execution'),
    ('delayed','Delayed'),
    ('postponed','Postponed'),
    ('closed','Closed'))

# [Project Model] - [ProjectLog Model] - Status of the project
project_status = (
    ('on_track','On track'),
    ('paused','Paused'),
    ('closed','Closed'))
