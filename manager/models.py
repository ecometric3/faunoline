from django.db import models
import datetime
from django.core import validators
from django.contrib.auth.models import User
from manager.utils import definitions, validators


class EcometricProject(models.Model):

    """
    + Clase: EcometricProject
        - Descripcion: Describe los proyectos existentes en Ecometric y estado
        - Relación EcometricProject [1-n] ProjectLog
        - Atributos:
            - start_date:  fecha inicio del proyecto
            - name: nombre del proyecto (FAUNOline, SONOline, etc)       
            - end_date:  fecha finalización del proyecto
            - project_manager:  nombre del gestor del proyecto
            - last_phase: última fase en la que está en el proyecto 
                - initiation: etapa inicial
                - planning: en planificación
                - developing: en desarrollo
                - in pilot: en piloto activo
                - execution: ejecutandose
                - delayed: retrasado
                - postponed: pospuesto
                - closed: cerrado
            - last_status:  ultimo estado
                - on track: initiation, planning, developing, in pilot, execution
                - paused: delayed(?), postponed
                - closed: closed
            - last_status_detail: detalle de la fase y estado del proyecto,
                                causas del estrado y proximos pasos
            - last_update:ultima actualización de la información 
    """
    # PK id auto django
    # Atributos
    name = models.CharField(
            max_length=45, 
            default=None,
            validators=validators.validator_project_name_length)
    start_date = models.DateField(auto_now=False)
    end_date = models.DateField(auto_now=False, blank=True, null=True)
    project_manager = models.CharField(
            max_length=45, 
            default=None,
            validators=validators.validator_manager_name,
            blank=True,
            null=True)
    last_phase = models.CharField(
            max_length = 10,
            choices = definitions.project_phases,
            default='initiation')
    last_status = models.CharField(
            max_length = 8,
            choices = definitions.project_status,
            default="on_track")
    last_status_detail = models.CharField(
            max_length=100, 
            default=None,
            validators=validators.validator_status_detail)
    last_update = models.DateField(auto_now=False, blank=True, null=True)
    
    def __str__(self):
        return '%s (%s)' % (self.name, self.pk)

    class Meta:
        verbose_name_plural = "Ecometric Projects"


class ProjectLog(models.Model):

    """
    + Clase ProjectLog: 
        - Descripcion: Describe el cambio de estado del proyecto
        - Relación EcometricProject [1-n] ProjectLog
        - Atributos:
            - ecometric_project_id = Foreign key - id del proyecto ecometric
            - date: fecha del cambio de estado registrado
            - phase: fase asociada a la fecha del log
                - initiation: etapa inicial
                - planning: en planificación
                - developing: en desarrollo
                - in pilot: en piloto activo
                - execution: ejecutandose
                - delayed: retrasado
                - postponed: pospuesto
                - closed: cerrado
            - status: estado asociado a la fecha del log
                - on track: initiation, planning, developing, in pilot, execution
                - paused: delayed(?), postponed
                - closed: closed
            - status_detail: detalle de la fase y estado del proyecto, causas
                        del estrado y proximos pasos 
    """
    # FK
    ecometric_project  = models.ForeignKey(
            EcometricProject,
            on_delete = models.CASCADE)
    # Atributos
    date = models.DateField(auto_now=False)
    phase = models.CharField(
            max_length = 10,
            choices = definitions.project_phases,
            default="initiation")
    status = models.CharField(
            max_length = 10,
            choices = definitions.project_status,
            default="on_track")
    status_detail = models.CharField(
            max_length=100, 
            default=None,
            validators=validators.validator_status_detail)

    def __str__(self):
        return '%s (%s)' % (self.ecometric_project.name, self.pk)

    class Meta:
        verbose_name_plural = "Ecometric Project Logs"

        
class Organization(models.Model):

    """
    + Clase Organization:
        - Descripcion: Organización a la que pertenece el proyecto
        - Relación: Organization [1-n] FaunoProject
        - Atributos:
            - name: nombre de la organización
            - contact_email: email de contacto oficial de la organización
            - contact_name: nombre del contacto oficial de la organización
            - contact_phone: teléfono de contacto oficial de la organización
    """
    # PK id auto django
    # Atributos
    name = models.CharField(
            max_length=45, 
            default=None,
            validators=validators.validator_org_name)
    contact_email= models.CharField(
            max_length=45, 
            default=None,
            validators=validators.validators_org_email)
    contact_name = models.CharField(
            max_length=45, 
            default=None,
            validators=validators.validators_org_contact)

    def __str__(self):
        return '%s (%s)' % (self.name, self.pk)

    class Meta:
        verbose_name_plural = "Organizations"


class FaunoProject(models.Model):

    """
    + Clase FaunoProject:
        - Descripcion: Proyectos de fauno asociados a la organización, del cual
                        se desprenden todas las estaciones asociadas. 
        - Relación: Organization [1-n] FaunoProject
        - Atributos:
            - organization_id: Foreign key - id de la organización
            - ecometric_project_id: Foreign key - id del proyecto ecometric
            - name: nombre asignado (Ej: faunokatalapi). Debe tener coherencia 
                con S3
            - start_date: fecha de inicio del proyecto con la organización
            - end_date: fecha de finalización del proyecto con la organización
            - description: descripción del proyecto con la organización 
                        (ej, objetivos específicos del cliente)
    """
    # Fk
    organization  = models.ForeignKey(
            Organization, 
            on_delete = models.CASCADE)
    ecometric_project = models.ForeignKey(
            EcometricProject,
            on_delete = models.CASCADE)
    # Atributos
    name = models.CharField(
            max_length=45, 
            default=None,
            validators=validators.validators_fauno_name)
    start_date = models.DateField(auto_now=False)
    end_date = models.DateField(auto_now=False, blank=True, null=True)
    description = models.CharField(
            max_length=100, 
            default=None,
            validators=validators.validator_fauno_description)
    
    def __str__(self):
        return '%s (%s)' % (self.name, self.pk)

    class Meta:
        verbose_name_plural = "Fauno Projects"

